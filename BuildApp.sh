#create bin folder
mkdir -p bin

#run cmake
cmake .

#build files
make

#copy execute files into roots bin directory
echo "Move execute files into bin directory" 
mv server/bin/server bin
mv client/bin/client bin

#clean up part
echo "Starting cleaning up"

#delete unused files in root folder
rm CMakeCache.txt cmake_install.cmake Makefile 
rm -rf CMakeFiles

#delete unused files in common folder
rm common/libcommon.a common/Makefile common/cmake_install.cmake 
rm -rf common/CMakeFiles

#delete unused files in server folder
rm server/Makefile server/cmake_install.cmake
rm -rf server/CMakeFiles server/bin server/common

#delete unused files in client folder
rm client/Makefile client/cmake_install.cmake
rm -rf client/CMakeFiles client/bin client/common

echo "Clean up DONE!"
