#include <iostream>
#include <cstring>
#include <thread>
#include <stdlib.h>
#include <chrono>
#include <fstream>
#include <string.h>
#include <system_error>

#include "../common/header/Message.h"
#include "../common/header/ClientSocketControl.h"

using namespace std;

bool isRunning = true;

void matrixCalculation(ClientSocketControl client) {
  //Wiadomość powitalna - połączenie z częscią obliczeniową. 
  //Zgłoszenie nowego węzła obliczeniowego 
  MSG helloMsg = {0, 5, 0, 0, 0, 0, 0, 0, 0, nullptr, nullptr, ""};
  int messageSize = Message::countSize(&helloMsg);
  char data[ messageSize ];
  helloMsg.messageSize = messageSize;
  
  //Serializacja wiadomości  
  Message::serialize(&helloMsg, data, 1);
  
  //Wysłanie wiadomości powitalnej
  bool rc = client.sendMessage(data, sizeof(data));
  if(!rc) {
    printf("Niepowodzenie wysłania wiadomości. Zakończenie programu");
    
    isRunning = false;
  }
  //this_thread::sleep_for(chrono::seconds(5));

  while (isRunning) {
    //Czekaj na wiadomość zwrotną z Serwera (macierze do pomnożenia)
    struct MSG msg = client.waitForMessage();
    
    if(isRunning == false || msg.msgId == -1) return;
    
    int result = 0;
    
    //Oblicz wynik dla konkretnej komórki
    for(int i = 0; i < msg.rowA; ++i) {
      result += msg.matrixA[0][i] * msg.matrixB[0][i];
    }
    
    //Utwórz wiadomość zwrotną
    msg.msgId = 3; 
    msg.result = result;
    msg.rowA = 0;
    msg.colA = 0;
    msg.matrixA = nullptr;
    msg.matrixB = nullptr;
    //MSG returnMessage = {0, 3, result, msg.clientId, msg.nodeId, 0, 0, msg.rowB, msg.colB, nullptr, nullptr, ""};
    
    //Oblicz rozmiar nowej wiadomości  
    messageSize = Message::countSizeForCalc(&msg);
    
    char data[ messageSize ];
    msg.messageSize = messageSize;
      
    Message::serialize(&msg, data, 1);
      
    //Odeślij wynik mnożenia
    bool rc = client.sendMessage(data, sizeof(data));
    if(!rc) {
      isRunning = false;
    }
  }
  
  //Zamknij wcześniej stworzone gniazdo
  client.closeSocket();
}

//Odczyt danych z pliku
void readFromFile(MSG* message) {
  fstream file;
  
  string fileName;
  printf("Podaj nazwę pliku, w którym znajdują się macierze: ");
  cin >> fileName;
  
  printf("\n");
  file.open( fileName, ios::in);
  
  if(file.good()) {
    string row;
    getline( file, row );
    
    int values[4] = {0}, 
        i=0;
    string temp;
 
    for(int j=0; j < (int)row.length(); ++j) {
      if(row[j] == ';') {
        values[i] = atoi(temp.c_str());
        temp = "";
        i++;
        
        continue;
      }

      temp += row[j];
    }
    
    if( values[1] == values[2] ) {
      message->rowA = values[0];
      message->colA = values[1];
      message->rowB = values[2];
      message->colB = values[3];
      message->msgId = 1;
      message->result = 0;
      message->clientId = 0;
      message->nodeId = 0;
      message->messageSize = 0;
      
      message->matrixA = (int**) malloc(message->rowA * sizeof(int*));
  
      for(int i = 0; i < message->rowA; ++i) {
          message->matrixA[i] = (int*) malloc(message->colA * sizeof(int));
      }
      for(int i = 0; i < message->rowA; ++i) {
        for(int j = 0; j < message->colA; ++j) {
          message->matrixA[i][j] = -1;
        }
      }
      
      message->matrixB = (int**) malloc(message->rowB * sizeof(int*));
      
      for(int i = 0; i < message->rowB; ++i) {
          message->matrixB[i] = (int*) malloc(message->colB * sizeof(int));
      }
      for(int i = 0; i < message->rowB; ++i) {
        for(int j = 0; j < message->colB; ++j) {
          message->matrixB[i][j] = -1;
        }
      }
      
      getline( file, row );
      
      int k = 0;
      for(int i=0; i < message->rowA; ++i) {
        getline( file, row );
        k=0;
        for(int j=0; j < (int)row.length() && k < message->colA; ++j) {
          if(row[j] == ';') {
            message->matrixA[i][k] = atoi(temp.c_str());
            temp = "";
            k++;
            
            continue;
          }
          
          temp += row[j];
        }
      }
     
      getline( file, row );
      k=0;
      for(int i=0; i < message->rowB; ++i) {
        getline( file, row );
        k=0;
        for(int j=0; j < (int)row.length() && k < message->colB; ++j) {
          if(row[j] == ';') {
            message->matrixB[i][k] = atoi(temp.c_str());
            temp = "";
            k++;
            
            continue;
          }
          
          temp += row[j];
        }
      }
      
      //Sprawdz poprawność uzupełnienia macierzy
      for(int i = 0; i < message->rowA; ++i) {
        for(int j = 0; j < message->colA; ++j) {
          if(message->matrixA[i][j] == -1) {
            message->msgId = -1;
            return;
          }
        }
      }
      
      for(int i = 0; i < message->rowB; ++i) {
        for(int j = 0; j < message->colB; ++j) {
          if(message->matrixB[i][j] == -1) {
            message->msgId = -1;
            return;
          }
        }
      }
      
    } else {
      message->msgId = -1;
    }
  } else {
    message->msgId = -1;
  }
}

//Zapis wyniku do pliku
void saveResult(MSG* message) {
  fstream file;
  string fileName;
  printf("Podaj nazwę pliku, do którego ma być zapisany wynik mnożenia: \n");
  cin >> fileName;
  file.open(fileName, std::ios::out);
  
  if(file.good()) {
    string temp;
    temp = to_string(message->rowA) + ";" + to_string(message->colA) + ";\n";
    file << temp;
    file << "\n";
    
    for(int i = 0; i < message->rowA; ++i) {
      temp = "";
      for(int j = 0; j < message->colA; ++j) {
        temp += to_string(message->matrixA[i][j]) + ";"; 
      }
      file << temp + "\n";
    }
    
    printf("Zapisano wynik do pliku!\n");
  } else {
    printf("Nie mozna zapisać pliku!\n");
    return;
  }
}

void sendMatrixes(char** argv) {
  MSG newMsg;
  
  //Wczytaj macierze z pliku
  readFromFile(&newMsg);

  if(newMsg.msgId == -1) {
    printf("Błąd odczytu pliku! Sprawdź lokalizację oraz zawartość\n");
    return;
  }
  //Gniazdo do komunikacji z częścią komunikacyjną Serwera
  ClientSocketControl client;
  client.connectToServer(argv[1], atoi(argv[3]));
  
  //Uzupełnienie wiadomości
  int messageSize = Message::countSize(&newMsg);
  newMsg.messageSize = messageSize;
  char data[messageSize]; 
  
  Message::serialize(&newMsg, data, 1);
    
  //Wyślij macierze do pomnożenia
  int rc = client.sendMessage(data, sizeof(data));
  if(!rc) {
    printf("Niepowodzenie wysłania wiadomości. Zakończenie programu");
    
    isRunning = false;
    return;
  }
  
  //Oczekuj na obdiór pomnożonych macierzy
  struct MSG msg = client.waitForMessage();
  
  //Zapisz wynik w pliku out.txt
  saveResult(&msg);
  
  //Zamknij gniazdo
  client.closeSocket();
}

int main(int argc, char** argv){
  if(argc != 4) {
    printf("Nie podano wymaganych parametrów (aresu IP serwera, portu części obliczeniowej, portu części komunikacyjnej\n");
    exit(-1);
  }
  
  int choosed = 0;
  
  //Gniazdo do komunikacji z częścią obliczeniową Serwera
  ClientSocketControl computationClient;
  computationClient.connectToServer(argv[1], atoi(argv[2]));
  
  //Stwórz wątek odpowiedziany na komunikację (obliczenia)
  //z częścią obliczeniową Serwera
  thread calculationThread;
  calculationThread = thread(matrixCalculation, computationClient);
  
  //Menu klienta
  while(isRunning) {
    printf("\nMENU GŁÓWNE: \n");
    printf("1. Wyślij macierze do pomnożenia\n");
    printf("2. Wyjdź z programu\n");
    
    cin >> choosed;
    
    switch(choosed) {
      case 1:
        sendMatrixes(argv);
        break;
      case 2:
        isRunning = false;
        break;
      default:
        break;
    }
  }
  
  computationClient.closeSocket();
  
  try
  {
    calculationThread.join();
  }
  catch( system_error const& err )
  {  
    cout << err.what() << endl;
  }
  
  printf("Klient zakończył pracę\n");
  
  return 0;
}
