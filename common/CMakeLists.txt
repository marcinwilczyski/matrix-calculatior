cmake_minimum_required(VERSION 2.8)

set(SHARED_LIB_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/header)

set(SHARED_LIB_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR} PARENT_SCOPE)

if(TARGET common)

message("common is already defined")

else()

include_directories(${SHARED_LIB_INCLUDE_DIR})

file (GLOB LIB_SRCS "./source/*.cpp")

add_library(common ${LIB_SRCS})

endif()