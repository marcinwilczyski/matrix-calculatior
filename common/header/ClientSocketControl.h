#ifndef __CLIENT_SOCKET_H_INCLUDED__  
#define __CLIENT_SOCKET_H_INCLUDED__   

#include "SocketControl.h"
#include "Message.h"

class ClientSocketControl : public SocketControl {
  public:
    ClientSocketControl();
    ~ClientSocketControl();
    
    void connectToServer(char* adr, int port);
    bool sendMessage(char* message, int size);
    struct MSG waitForMessage();
};

#endif 