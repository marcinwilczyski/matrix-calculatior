#ifndef __MESSAGE_INCLUDED__
#define __MESSAGE_INCLUDED__

struct MSG {
    int messageSize;
    int msgId;
    int result;
    int clientId;
    int nodeId;
    int rowA;
    int colA;
    int rowB;
    int colB;
    
    int** matrixA;
    int** matrixB;
    
    char msg[100];
};

class Message {
    public:
      Message();
      ~Message();
      
      static void serialize(MSG* message, char* data, int option);
      static void deserialize(char* data, MSG* message);
      static void printMsg(MSG* message);
      static int countSize(MSG* message);
      static int countSizeForCalc(MSG* message);
};
#endif 