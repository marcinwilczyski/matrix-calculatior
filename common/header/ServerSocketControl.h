#ifndef __SERVER_SOCKET_H_INCLUDED__  
#define __SERVER_SOCKET_H_INCLUDED__   

#include "SocketControl.h"
#include "Message.h"

class ServerSocketControl : public SocketControl {
  public:
    ServerSocketControl();
    ~ServerSocketControl();
    
    void createServer(int port);
    struct MSG waitForMessage();
    bool sendMessageTo(int to, char* message, int size);
    struct MSG readMessageFrom(int from);
};

#endif 