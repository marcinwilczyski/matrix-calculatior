#ifndef __SOCKET_H_INCLUDED__  
#define __SOCKET_H_INCLUDED__   

#include "Message.h"

class SocketControl {
  protected:
    int socketNumber;
    
  public:
    SocketControl();
    ~SocketControl();
    
    void closeConnection(int desc);
    void closeSocket();
    
    int getSocketNumber();
    int ReadSize(int socket);
    bool ReadXBytes(int socket, unsigned int x, char* buffer);
};

#endif 