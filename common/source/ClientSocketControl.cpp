#include "../header/ClientSocketControl.h"
#include "../header/Message.h"
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>

using namespace std;

ClientSocketControl::ClientSocketControl() { }

ClientSocketControl::~ClientSocketControl() { }

//Połaczenie z serwerem
void ClientSocketControl::connectToServer(char* adr, int port){
  this->socketNumber = socket(AF_INET, SOCK_STREAM , 0);
  if (this->socketNumber < 0)
  {
    exit(-1);
  }
  sockaddr_in address;
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = inet_addr(adr);
  address.sin_port = htons(port);
  int rc = connect(this->socketNumber, (sockaddr*)(&address), sizeof(sockaddr));
  if (rc < 0)
  {
    perror("Niepowodzenie funkcji connect()");
    close(this->socketNumber);
    exit(-1);
  }
}

//Wysłanie wiadomości na serwer
bool ClientSocketControl::sendMessage(char* message, int size){
  while(size > 0) {
    int sentSize = write(this->socketNumber, message, size);
    if(sentSize == -1) return false;
    
    message += sentSize;
    size -= sentSize;
  }
  
  return true;
}

//Oczekiwanie na wiadomość z serwera
struct MSG ClientSocketControl::waitForMessage() {
  unsigned int length = 0;
  char* buffer = 0;
  MSG msg;
  bool rc = false;
  
  length = ReadSize(this->socketNumber);
  
  if(length > 0) {
    buffer = new char[length];
    rc = ReadXBytes(this->socketNumber, length, buffer);
  }
  
  if(rc == false) {
    MSG msg = {0, -1, 0, 0, 0, 0, 0, 0, 0, nullptr, nullptr, ""};
    delete [] buffer;

    return msg;
  }
  else {
    Message::deserialize(buffer, &msg);
  }
  
  delete [] buffer;
  
  return msg;
}
