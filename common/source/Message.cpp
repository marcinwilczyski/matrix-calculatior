#include "../header/Message.h"
#include <iostream>

using namespace std;

Message::Message(){ }

Message::~Message(){ }

//Serializacja danych (do jednego ciągu znaków)
void Message::serialize(MSG* message, char* data, int option) {
    int *q = (int*)data;
    
    *q = message->messageSize; q++;
    *q = message->msgId;      q++;
    *q = message->result;     q++;
    *q = message->clientId;   q++;
    *q = message->nodeId;     q++;
    *q = message->rowA;       q++;
    *q = message->colA;       q++;
    *q = message->rowB;       q++;
    *q = message->colB;       q++;
    
    if (message->rowA !=0) {
    if(option == 1) {
      for (int i=0; i < message->rowA; ++i) {
          for (int j=0; j < message->colA; ++j) {
              *q = message->matrixA[i][j];
              q++;
          }
      }
      
      for (int i=0; i < message->rowB; ++i) {
          for (int j=0; j < message->colB; ++j) {
              *q = message->matrixB[i][j];
              q++;
          }
      }
    }
    
    if(option == 2) {
      
          for (int j=0; j < message->rowA; ++j) {
              *q = message->matrixA[0][j];
              q++;
          }
      
      
      
          for (int j=0; j < message->rowA; ++j) {
              *q = message->matrixB[0][j];
              q++;
          }
      
    }
    }
    int i = 0;
    char *p = (char*)q;
    while (i < 100)
    {
        *p = message->msg[i];
        p++;
        i++;
    }
    
    p++;
    *p = '\0';
}

//Deserializacja danych (do struktury MSG)
void Message::deserialize(char* data, MSG* message) {
    int *q = (int*)data;
      
    message->msgId = *q;      q++;
    message->result = *q;     q++;
    message->clientId = *q;   q++;
    message->nodeId = *q;     q++;
    message->rowA = *q;       q++;
    message->colA = *q;       q++;
    message->rowB = *q;       q++;
    message->colB = *q;       q++;
    
    
    if(message->msgId != 2) {
      message->matrixA = (int**) malloc(message->rowA*sizeof(int*));
      for (int i=0; i < message->rowA; ++i) {
          message->matrixA[i] = (int*) malloc(message->colA * sizeof(int));
      }
      
      for (int i=0; i < message->rowA; ++i) {
          for (int j=0; j < message->colA; ++j) {
              message->matrixA[i][j] = *q;
              q++;
          }
      }
      
      message->matrixB = (int**) malloc(message->rowB*sizeof(int*));
      for (int i=0; i < message->rowB; ++i) {
          message->matrixB[i] = (int*) malloc(message->colB * sizeof(int));
      }
      
      for (int i=0; i < message->rowB; ++i) {
          for (int j=0; j < message->colB; ++j) {
              message->matrixB[i][j] = *q;
              q++;
          }
      }
    } else {
      message->matrixA = (int**) malloc(sizeof(int*));
      message->matrixA[0] = (int*) malloc(message->rowA * sizeof(int));
      
      
          for (int j=0; j < message->rowA; ++j) {
            message->matrixA[0][j] = *q;
            q++;
          }
      
      
      message->matrixB = (int**) malloc(sizeof(int*));
      message->matrixB[0] = (int*) malloc(message->rowA * sizeof(int));
      
      
          for (int j=0; j < message->rowA; ++j) {
              message->matrixB[0][j] = *q;
              q++;
          }
      
    }
    
    int i = 0;
    char *p = (char*)q;
    while (i < 100)
    {
      message->msg[i] = *p;
      p++;
      i++;
    }
}    
        
//Wyświetlenie macierzy
void Message::printMsg(MSG* message) {
    cout << "Identyfikator widomości: " << message->msgId << endl;
    cout << "Wynik mnożenia: " << message->result << endl;
    cout << "Identyfikator macierzy: " << message->clientId << endl;
    cout << "Rozmiar macierzy A: " << message->rowA << "x" << message->colA << endl;
    
    cout << endl << "Macierz A: " << endl;
    for (int i=0; i < message->rowA; ++i) {
        for (int j=0; j < message->colA; ++j) {
            cout << message->matrixA[i][j] << " ";
        }
        cout << endl;
    }
    
    cout << endl << "Rozmiar macierzy B: " << message->rowB << "x" << message->colB << endl;
   
    cout << endl << "Macierz B: " << endl;
    for (int i=0; i < message->rowB; ++i) {
        for (int j=0; j < message->colB; ++j) {
            cout << message->matrixB[i][j] << " ";
        }
        cout << endl;
    }
    
    cout << endl << "Wiadomość: " << message->msg << endl;
}

//Obliczenie wielkości wiadomości wierającej
//dwie pełnowymiarowe macierze
int Message::countSize(MSG* message) {
    return ((message->rowA * message->colA) + (message->rowB * message->colB) + 9) 
            * sizeof(int) + sizeof(message->msg) + 1;
}

//Obliczenie wielkości wiadomości zawierającej
//wiersz i kolumnę do pomnożenia
int Message::countSizeForCalc(MSG* message) {
    return ((2 * message->rowA) + 9) * sizeof(int) + sizeof(message->msg) + 1;
}