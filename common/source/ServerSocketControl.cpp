#include "../header/ServerSocketControl.h"
#include "../header/Message.h"
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string>

using namespace std;

ServerSocketControl::ServerSocketControl() { }

ServerSocketControl::~ServerSocketControl() { }

//Tworzenie serwera
void ServerSocketControl::createServer(int port) {
  this->socketNumber = socket(AF_INET, SOCK_STREAM , 0);
  if (this->socketNumber < 0)
  {
    perror("Niepowodzenie funkcji socket()");
    exit(-1);
  }
  sockaddr_in sockaddress;
  sockaddress.sin_family = AF_INET;
  sockaddress.sin_port = htons(port);
  sockaddress.sin_addr.s_addr = htons(INADDR_ANY);
  int rc = ::bind(this->socketNumber, (sockaddr*)(&sockaddress), sizeof(sockaddr));
  if (rc < 0)
  {
    perror("Niepowodzenie funkcji bind()");
    close(this->socketNumber);
    exit(-1);
  }
  rc = listen(this->socketNumber, 10);
  if (rc < 0)
  {
    perror("Niepowodzenie funkcji listen()");
    close(this->socketNumber);
    exit(-1);
  }
}

//Odczyt wiadomości z socketu
//przyłączenie nowego połączenia
struct MSG ServerSocketControl::waitForMessage() {
  unsigned int length = 0;
  char* buffer = 0;
  MSG msg;
  bool rc = false;
    
  sockaddr_in address;
  socklen_t size;
    
  int connectionDescriptor = accept(this->socketNumber,(sockaddr*)(&address), &size);
    
  length = ReadSize(connectionDescriptor);
  
  if(length > 0) {
    buffer = new char[length];
    rc = ReadXBytes(connectionDescriptor, length, buffer);
  }
  
  if(rc == false) {
    MSG msg = {0, -1, 0, 0, 0, 0, 0, 0, 0, nullptr, nullptr, ""};
    delete [] buffer;
    return msg;
  } 
  else {
    Message::deserialize(buffer, &msg);
    msg.clientId = connectionDescriptor;
  }
 
  delete [] buffer;
  
  return msg;
}

//Odczyt wiadomości od konkretnego węzła
struct MSG ServerSocketControl::readMessageFrom(int from){
  unsigned int length = 0;
  char* buffer = 0;
  MSG msg;
  bool rc = false;
  
  length = ReadSize(from);

  if(length > 0) {
    buffer = new char[length];
    rc = ReadXBytes(from, length, buffer);
  }
  
  if(rc == false) {
    MSG msg = {0, -1, 0, 0, 0, 0, 0, 0, 0, nullptr, nullptr, ""};
    delete [] buffer;
    return msg;
  }
  else {
    Message::deserialize(buffer, &msg);
  } 
  
  delete [] buffer;
  
  return msg;
}

//Wysłanie wiadomości do konkretnego węzła obliczeniowego
bool ServerSocketControl::sendMessageTo(int to, char* message, int size){
  while(size > 0) {
    int sentSize = write(to, message, size);
    if(sentSize == -1) return false;
    
    message += sentSize;
    size -= sentSize;
  }
  
  return true;
}
