#include "../header/SocketControl.h"
#include "../header/Message.h"
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string>

using namespace std;

SocketControl::SocketControl(){ }

SocketControl::~SocketControl(){ }

//Zakończ działanie socketu serwera
void SocketControl::closeSocket(){
    close(this->socketNumber);
}

//Zakończ działanie socketu połączonego
//węzła obliczeniowego
void SocketControl::closeConnection(int desc){
    close(desc);
}

//Pobierz numer socketu stworzonego serwera
int SocketControl::getSocketNumber() {
    return this->socketNumber;
}

//Odczytaj rozmiar przychodzącej wiadomości,
//którym jest pierwszy element odbieranych danych
int SocketControl::ReadSize(int socket) {
  char buffer[4];
  
  read(socket, buffer, 4);
  int* s = (int*)buffer;

  return *s - 4;
}

//Odbierz całą wiadomość z określonego socketu
bool SocketControl::ReadXBytes(int socket, unsigned int x, char* buffer) {
  int result;
  
  while(x > 0) {
    result = read(socket, buffer , x);
    if (result < 1 ) {
      return false;         
    }

    buffer += result;
    x -= result;
  }
  
  return true;
}
