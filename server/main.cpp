#include <iostream>
#include <vector>
#include <utility>
#include <deque>
#include <thread>
#include <poll.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <chrono>
#include <utility>
#include <cstring>
#include <algorithm> 
#include <system_error>

#include "../common/header/ServerSocketControl.h"
#include "../common/header/Message.h"

using namespace std;

struct serverMSG {
  MSG msg;
  int** resultMatrix;
  int howMany;
};

//Zmienne
deque<int> readyToCalculate;

vector< serverMSG > clientsMessage;
bool isRunning = true;

struct pollfd computationFDS[200];
int numberOfComputationFDS = 2;

//Stwórz strukturę z odebranej wiadomości (macierze do pomnożenia)
//na potrzeby przetwarzania po stronie serwera
void makeStructure(MSG* receivedMessage) {
  serverMSG message;
  
  message.msg.messageSize = 0;
  message.msg.msgId = 1;
  message.msg.result = 0;
  message.msg.clientId = receivedMessage->clientId;
  message.msg.nodeId = 0;
  message.msg.rowA = receivedMessage->rowA;
  message.msg.colA = receivedMessage->colA;

  message.msg.matrixA = (int**) malloc(message.msg.rowA * sizeof(int*));
  for(int k = 0; k < message.msg.rowA; ++k) {
    message.msg.matrixA[k] = (int*) malloc(message.msg.colA * sizeof(int));
  }

  for(int i = 0; i < receivedMessage->rowA; ++i) {
    for (int j = 0; j < receivedMessage->colA; ++j) {
      message.msg.matrixA[i][j] = receivedMessage->matrixA[i][j];
    }
  }

  message.msg.rowB = receivedMessage->rowB;          
  message.msg.colB = receivedMessage->colB;
  
  message.msg.matrixB = (int**) malloc(message.msg.rowB * sizeof(int*));
  for(int k = 0; k < message.msg.rowB; ++k) {
    message.msg.matrixB[k] = (int*) malloc(message.msg.colB * sizeof(int));
  }

  for(int i = 0; i < receivedMessage->rowB; ++i) {
    for (int j = 0; j < receivedMessage->colB; ++j) {
      message.msg.matrixB[i][j] = receivedMessage->matrixB[i][j];
    }
  }
  
  strcpy(message.msg.msg, "");

  message.resultMatrix = (int**) malloc(receivedMessage->rowA * sizeof(int*));
  for(int i = 0; i < receivedMessage->rowA; ++i) {
    message.resultMatrix[i] = (int*) malloc(receivedMessage->colB * sizeof(int));
  }
  
  for(int i = 0; i < receivedMessage->rowA; ++i) {
    for (int j = 0; j < receivedMessage->colB; ++j) {
      message.resultMatrix[i][j] = -2;
    }
  }
  
  message.howMany = receivedMessage->rowA * receivedMessage->colB;
  
  clientsMessage.push_back(message);
}

//Przejrzyj wektor odebranych wiadomości w poszukiwaniu
//nie obliczonego jeszcze elementu
struct MSG getElementToSend() {
  for(struct serverMSG& item : clientsMessage) {
    if(item.howMany > 0) {
      for(int i = 0; i < item.msg.rowA; ++i) {
        for (int j = 0; j < item.msg.colB; ++j) {
          if(item.resultMatrix[i][j] == -2) {
            MSG msg;
            
            msg.matrixA = (int**) malloc(sizeof(int*));
            msg.matrixA[0] = (int*) malloc(item.msg.colA * sizeof(int));
 
            for(int k = 0; k < item.msg.colA; ++k) {
              msg.matrixA[0][k] = item.msg.matrixB[i][k];
            }
            
            msg.matrixB = (int**) malloc(sizeof(int*));
            msg.matrixB[0] = (int*) malloc(item.msg.rowB * sizeof(int));

            for(int k = 0; k < item.msg.rowB; ++k) {
                msg.matrixB[0][k] = item.msg.matrixB[k][j];
            }

            msg.messageSize = 0;
            msg.msgId = 2;
            msg.result = 0;
            msg.clientId = item.msg.clientId;
            msg.nodeId = 0;
            msg.rowA = item.msg.colA;
            msg.colA = item.msg.rowB;
            msg.rowB = i;
            msg.colB = j;
            strcpy(msg.msg, "");
            
            item.resultMatrix[i][j] = -1;
            
            return msg;
          }
        }
      }
    }
  }
  
  MSG msg = {0, -1, 0, 0, 0, 0, 0, 0, 0, nullptr, nullptr, ""};
  
  return msg;
}

//Porządkuj tablicę przyłączonych węzłów obliczeniowych
void compressArray() {
  for (int i = 0; i < numberOfComputationFDS; ++i) {
    if (computationFDS[i].fd == -1)
    {
      for(int j = i; j < numberOfComputationFDS; ++j)
      {
        computationFDS[j].fd = computationFDS[j+1].fd;
      }
      numberOfComputationFDS--;
    }
  }
}

//Wyślij wektory do węzła obliczeniowego
//Odeślij macierz, jeeli ta jest jest już obliczona
void sendMatrices(ServerSocketControl *server) {
  while(isRunning) {
    if(clientsMessage.size() > 0 && readyToCalculate.size() > 0) {
      struct MSG msg = getElementToSend();
       
      if(msg.msgId != -1) {
        msg.nodeId = readyToCalculate.front();
        readyToCalculate.pop_front();
  
        int messageSize = Message::countSizeForCalc(&msg);
        char data[ messageSize ];
        msg.messageSize = messageSize;
        
        Message::serialize(&msg, data, 2);
        
        bool rc = server[0].sendMessageTo(msg.nodeId, data, sizeof(data));
        if(!rc) {
          printf("Błąd węzła obliczeniowego - zamknięto połączenie: %d \n", msg.nodeId);
          for(struct serverMSG& it : clientsMessage) {
            if(it.msg.clientId == msg.clientId) {
              it.resultMatrix[msg.rowB][msg.colB] = -2;
            }
          }
          
          server[0].closeConnection(msg.nodeId);
          
          for (int i = 0; i < numberOfComputationFDS; i++) {
            if(computationFDS[i].fd == msg.nodeId) {
              computationFDS[i].fd = -1;
              compressArray();
              
              break;
            }
          }
        } 
      } 
    }
    
    
    int i = 0;
    bool deleteMatrix = false;
    for(struct serverMSG& item : clientsMessage) {
      if(item.howMany == 0) {
        MSG msg;
        msg.messageSize = 0;
        msg.msgId = 4;
        msg.result = 0;
        msg.clientId = item.msg.clientId;
        msg.nodeId = 0;
        msg.rowA = item.msg.rowA;
        msg.colA = item.msg.colB;
        msg.rowB = 0;
        msg.colB = 0;
        
        msg.matrixA = (int**) malloc(msg.rowA * sizeof(int*));
        for(int i = 0; i < msg.rowA; ++i) {
          msg.matrixA[i] = (int*) malloc(msg.colA * sizeof(int));
        }
        
        for(int i = 0; i < msg.rowA; ++i) {
          for(int j = 0; j < msg.colA; ++j) {
            msg.matrixA[i][j] = item.resultMatrix[i][j];
          }
        }
        
        int messageSize = Message::countSize(&msg);
        char data[ messageSize ];
        msg.messageSize = messageSize;
        
        Message::serialize(&msg, data, 1);
           
        bool rc = server[1].sendMessageTo(msg.clientId, data, sizeof(data));
        if(rc) {
          printf("Odeslano pomnożone macierze: %d \n", msg.clientId);
        } else {
          printf("Błąd odbioru wymnożonych macierzy przez klienta: %d \n", msg.clientId);
        }
        
        server[1].closeConnection(msg.clientId);
        deleteMatrix = true;
        break;
      }
      ++i;
    }
     
    if(deleteMatrix) clientsMessage.erase(clientsMessage.begin() + i);
  }
}

//Sprawdź, czy któryś z węzłów po upływie 10s nadal liczy swoje zadania
//(duże przwdopodobieństwo awarii węzła), jeżeli tak, 
//to zmień status elementu na -2, zostanie wtedy wysłany do innego węzła
void changeMatrixElementsStatus() {
  for(struct serverMSG& item : clientsMessage) {
    for(int i = 0; i < item.msg.rowA; ++i) {
      for (int j = 0; j < item.msg.colB; ++j) {
        if(item.resultMatrix[i][j] == -1) {
          item.resultMatrix[i][j] = -2;
        }
      }
    }
  }
}

//Odbierz pomnożone wektory od węzła obliczeniowego
void getResultFromNodes(ServerSocketControl *servers) {
  while(isRunning) {
    int rc = poll(computationFDS, numberOfComputationFDS, 10 * 1000);

    if (rc < 0) {
      printf("Niepowodzenie funkcji poll(). Serwer zostanie wyłączony!\n");
      isRunning = false;
      break;
    }

    else if (rc == 0) {
      printf("Nie odebrano zadnej wiadomosci w ciagu ostanich 10 sekund!\n");
      changeMatrixElementsStatus();
    }
    
    else {
      bool needArrayCompress = false;
      
      for (int i = 0; i < numberOfComputationFDS; i++) {
        if(computationFDS[i].revents == 0) continue;

        if(computationFDS[i].fd == servers[0].getSocketNumber()) {
          struct MSG msg = servers[0].waitForMessage();
              
          if(msg.msgId == -1) {
            isRunning = false;
            
            break;
          }
          
          if(msg.msgId == 5) {
            computationFDS[numberOfComputationFDS].fd = msg.clientId;
            computationFDS[numberOfComputationFDS].events = POLLIN;
            numberOfComputationFDS++;
            
            readyToCalculate.push_back(msg.clientId);
            printf("Podlaczono nowy wezel obliczeniowy: %d \n", msg.clientId);
          }
        } else if(computationFDS[i].fd == servers[1].getSocketNumber()) {
          struct MSG msg = servers[1].waitForMessage();
       
          if(msg.msgId != -1 && msg.msgId == 1) {
            makeStructure(&msg);
            
            printf("Odebrano macierze do pomnozenia: %d \n", msg.clientId);
          }
        } else {
          struct MSG msg = servers[0].readMessageFrom(computationFDS[i].fd);
         
          if(msg.msgId == -1) {
            printf("Błąd w odczycie danch!\n");
            readyToCalculate.erase(remove(readyToCalculate.begin(), readyToCalculate.end(), computationFDS[i].fd), readyToCalculate.end());
            
            servers[0].closeConnection(computationFDS[i].fd);
            computationFDS[i].fd = -1;
            
            needArrayCompress = true;
          }
          
          if(msg.msgId  == 3) {
            for(struct serverMSG& it : clientsMessage) {
              if(it.msg.clientId == msg.clientId) {
                printf("Węzel obliczeniowy zwrócił obliczoną wartość: %d %d %d\n", msg.nodeId, msg.rowB, msg.colB);
                it.resultMatrix[msg.rowB][msg.colB] = msg.result;
                
                it.howMany -= 1;   
                
                readyToCalculate.push_back(msg.nodeId); 
    
                break;
              }
            }
          }
          
          if(needArrayCompress) compressArray();
        }
      }
    }
  }
}

int main(int argc, char** argv) {
  if(argc != 3) {
    printf("Nie podano wymaganych parametrów (portu części obliczeniowej, portu części komunikacyjnej\n");
    exit(-1);
  }
  
  //Stwórz socket odpowiadający za część obliczeniową
  ServerSocketControl cServer;
  cServer.createServer(atoi(argv[1]));
  
  //Stwórz socket odpowiadający za część komunikacyjną
  ServerSocketControl cmServer;
  cmServer.createServer(atoi(argv[2]));
  
  memset(computationFDS, 0 , sizeof(computationFDS));
  computationFDS[0].fd = cServer.getSocketNumber();
  computationFDS[0].events = POLLIN;
  
  computationFDS[1].fd = cmServer.getSocketNumber();
  computationFDS[1].events = POLLIN;
  
  thread sendMatricesThread, getResultFromNodesThread;
    
  ServerSocketControl servers[2];
  servers[0] = cServer;
  servers[1] = cmServer;
  
  sendMatricesThread = thread(sendMatrices, servers);
  getResultFromNodesThread = thread(getResultFromNodes, servers);
  
  while(113 != getchar()) { } //q - quit
  isRunning = false;
  
  //Po zakończeniu pracy serwera zamknij wszystkie otwarte 
  //deskryptory
  for(auto it : clientsMessage) {
    cServer.closeConnection(it.msg.clientId);
  }
  
  for (int i = 0; i < numberOfComputationFDS; i++) {
    cServer.closeConnection(computationFDS[i].fd);
  }
  
  cServer.closeSocket();
  cmServer.closeSocket();
  
  try
  {
    sendMatricesThread.join();
    getResultFromNodesThread.join();
  }
  catch( system_error const& err )
  {  
    cout << err.what() << endl;
  }
  
  printf("Serwer zakończył pracę\n");
  
  return 0;
}
